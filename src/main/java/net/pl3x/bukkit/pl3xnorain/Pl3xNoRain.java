package net.pl3x.bukkit.pl3xnorain;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class Pl3xNoRain extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        saveDefaultConfig();

        Bukkit.getPluginManager().registerEvents(this, this);

        new BukkitRunnable() {
            @Override
            public void run() {
                for (World world : Bukkit.getWorlds()) {
                    if (allowWorldRain(world)) {
                        continue;
                    }
                    world.setThundering(false);
                    world.setStorm(false);
                    world.setThunderDuration(0);
                    world.setWeatherDuration(Integer.MAX_VALUE);
                }
            }
        }.runTaskLater(this, 20);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onWeatherChange(WeatherChangeEvent event) {
        World world = event.getWorld();
        if (allowWorldRain(world)) {
            return;
        }
        if (event.toWeatherState()) {
            event.setCancelled(true);
        }
    }

    private boolean allowWorldRain(World world) {
        return !getConfig().getStringList("no-rain-worlds").contains(world.getName());
    }
}
